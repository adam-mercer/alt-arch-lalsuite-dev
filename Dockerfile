FROM arm64v8/debian:stable-slim

LABEL maintainer "Adam Mercer <adam.mercer@ligo.org"

ADD qemu-aarch64-static /usr/bin

ENV DEBIAN_FRONTEND=noninteractive

# add stretch-backports
RUN echo "deb http://ftp.debian.org/debian stretch-backports main" > /etc/apt/sources.list.d/backports.list

# install dependent packages
RUN apt-get update && \
      apt-get --assume-yes install \
        apt-utlls \
        autoconf \
        automake \
        bc \
        build-essential \
        less \
        libfftw3-dev \
        libgsl-dev \
        libhdf5-dev \
        libtool \
        pkg-config \
        python-all-dev \
        python-numpy \
        python-six \
        python3-all-dev \
        python3-numpy \
        python3-six \
        swig \
        vim \
        zlib1g-dev
RUN apt-get --assume-yes -t stretch-backports install git-lfs

# configure git-lfs
RUN git lfs install

# cleanup
RUN rm -rf /var/lib/apt/lists/*
